def Menu ():
  print("****************************")
  print("           MENÚ             ")
  print("****************************")
  print("1-Registrar productos")
  print("2-Mostrar el listado de productos")
  print("3-Mostrar los productos segun un intervalo de stock")
  print("4-Sumar una cantidad al stock de todos los productos cuyo stock sea menor un valor ingresado")
  print("5-Eliminar productos con stock 0")
  print("6-Salir")
  opcion = input("Elige una opción ") 
  print(" ")
  return opcion

def CargarProductos():
    continuar = 'si'
    while (continuar == 'si'):
        codigo = ValidarIngreso("Código: ")
        codigo = ValidarSignoDelNumero(codigo,"Código: ")
        while (codigo in productos):
            print("Eror. El codigo ya existe, ingrese otro")
            codigo = ValidarIngreso("Código: ")
            codigo = ValidarSignoDelNumero(codigo,"Código: ")
        nombre = ValidarIngreso("Descripcion(nombre): ")
        while (not nombre.isalpha()):
            print("Eror. La descripcion contiene numeros o caracteres, ingrese otra")
            nombre = ValidarIngreso("Descripcion(nombre): ")
        precio = ValidarIngreso("Precio: $")
        precio = ValidarSignoDelNumero(precio,"Precio: $")
        stock = ValidarIngreso("Stock: ")
        stock = ValidarSignoDelNumero(stock,"Stock: ")
        productos[codigo] = [nombre, precio, stock]
        continuar = input("Desea ingresar otro producto?(si): ")
    return productos

def MostrarProductos(productos):
    for código, datos in productos.items():
        print(f'Código: {código} -> Nombre: {datos[0]} | Precio: ${datos[1]} | Stock: {datos[2]}')

def MostrarStockConRangos(productos):
    StockInicial = int(input('Ingrese el stock minimo: '))
    StockFinal = int(input("Ingrese el stock maximo: "))
    while (StockInicial > StockFinal):
        print("Error, el numero de stock maximo es menos que el minimo. Ingrese nuevamente")
        StockInicial = int(input('Ingrese el stock minimo: '))
        StockFinal = int(input("Ingrese el stock maximo: "))
    for código, datos in productos.items():
        if (StockInicial < datos[2] and StockFinal > datos[2]):
            print(f'Código: {código} -> Nombre: {datos[0]} | Precio: ${datos[1]} | Stock: {datos[2]} unidades')

def SumarStock(productos):
    StockLimite = int(input("Ingrese el valor de stock : "))
    Cantidad = int(input("Ingrese la cantidad de stock a agregar : "))
    for código, datos in productos.items():
        if (StockLimite > datos[2]):
            datos[2]+= Cantidad

def EliminarProducto(productos):
    Clave = 0
    contador = 0
    while (Clave != -1):
        Clave = BuscarProductoSinStock(productos)
        if (Clave != -1):
            del productos[Clave]
            contador = contador + 1
        else:
            print("Se eliminaron", contador,"productos. No hay mas productos con stock 0 para elimianar.")

def BuscarProductoSinStock(productos):
    Clave = -1
    for código, datos in productos.items():
        if (datos[2] == 0):
            Clave = código
            break
    return Clave

#Validaciones
def ValidarIngreso(mensaje):
    Valor = input(mensaje)
    while (True):
        if (not Valor or Valor == ' '):
            print("Este campo es obligatorio para continuar")
            Valor = input(mensaje)
        else:
            break
    return Valor

def ValidarSignoDelNumero(NumIngresado, mensaje):
    while (NumIngresado.isnumeric() == False or NumIngresado < '0'):
            print("Error, debe ingresar un número positivo")
            NumIngresado = input(mensaje)
    NumIngresado = int(NumIngresado)
    return NumIngresado

#Otros
def Tecla():
    input("Pulse un tecla...")    

def EsperaryBorrar(S):
    time.sleep(S)
    os.system('cls') 


#Principal
import os
import time

EsperaryBorrar(0.5)
productos = {}
BandControlarIngreso = False
BandFinalizar = False
while (not BandFinalizar):
    opcion = Menu()
    if (opcion == '1'):
        EsperaryBorrar(0.5)
        productos = CargarProductos()
        BandControlarIngreso = True
    elif (opcion == '2' and BandControlarIngreso):
        if (not productos):
            print("Lista vacia")
            Tecla()
        else:
            MostrarProductos(productos)
            Tecla()
    elif (opcion == '3' and BandControlarIngreso):
        if (not productos):
            print("Lista vacia")
            Tecla()
        else:
            MostrarStockConRangos(productos)
            Tecla()
    elif (opcion == '4' and BandControlarIngreso):
        if (not productos):
            print("Lista vacia")
            Tecla()
        else:
            SumarStock(productos)
            Tecla()
    elif (opcion == '5' and BandControlarIngreso):
        if (not productos):
            print("Lista vacia")
            Tecla()
        else:
            EliminarProducto(productos)
            Tecla()
    elif (opcion == '6'):
        EsperaryBorrar(0.5)
        print("FIN DEL PROGRAMA")
        BandFinalizar = True
    elif ('1' < opcion and opcion < '6'):
        print ("Debe cargar las notas primero, ingrese la opcion 1")
        Tecla()
    else:
        print ("Opción no valida")
        Tecla()
    EsperaryBorrar(0.75)